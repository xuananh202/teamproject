from django.conf.urls.static import static
from django.urls import include, path

from config import settings
from . import views
from django.conf.urls import url


urlpatterns = [
    url('', include("django.contrib.auth.urls")),
    path('', views.index, name="index"),
    path('register/', views.register, name='register'),
    path('<int:user_id>/customer_info', views.customer_info, name='customer_info'),
    path('profile', views.profile, name = 'profile'),
    path('profile/update', views.profile_update, name = 'profile_update'),
    path('item/add', views.add, name = 'add'),
    path('item/<int:product_id>/detail', views.detail, name='detail'),
    path('item/<int:product_id>/delete', views.product_delete, name='product_delete'),
    path('item/<int:product_id>/checkout', views.checkout, name='checkout'),
    path('<int:purchase_id>/confirm', views.confirm, name='confirm'),
    path('<int:purchase_id>/purchaselog', views.purchaselog, name='purchaselog'),
    path('<int:purchase_id>/delete', views.purchaselog_delete, name='delete'),
    path('purchase/<int:purchase_id>', views.purchase, name='purchase'),
    path('payment_done/<int:purchase_id>', views.payment_done, name = 'done'),
    path('payment_cancelled/<int:purchase_id>', views.payment_cancelled, name = 'cancelled')
]
