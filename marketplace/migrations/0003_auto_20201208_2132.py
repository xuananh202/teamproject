# Generated by Django 3.1.3 on 2020-12-08 14:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('marketplace', '0002_purchase_log'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Purchase_Log',
            new_name='PurchaseLog',
        ),
    ]
