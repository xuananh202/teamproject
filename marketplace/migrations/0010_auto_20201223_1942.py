# Generated by Django 3.1.3 on 2020-12-23 10:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marketplace', '0009_purchaselog_seller'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='business_email',
            field=models.EmailField(blank=True, max_length=254, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='personal_email',
            field=models.EmailField(default='sb-b7nqp4205204@personal.example.com', max_length=254),
            preserve_default=False,
        ),
    ]
