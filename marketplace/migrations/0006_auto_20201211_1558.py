# Generated by Django 3.1.3 on 2020-12-11 08:58

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('marketplace', '0005_remove_purchaselog_product'),
    ]

    operations = [
        migrations.AddField(
            model_name='purchaselog',
            name='notes',
            field=models.TextField(default='none'),
        ),
        migrations.AddField(
            model_name='purchaselog',
            name='payment_method',
            field=models.TextField(default=None),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='purchaselog',
            name='posted_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='purchaselog',
            name='product',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='purchase_log', to='marketplace.product'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='purchaselog',
            name='purchase_number',
            field=models.IntegerField(default=1),
        ),
        migrations.AddField(
            model_name='purchaselog',
            name='sum',
            field=models.IntegerField(default=None),
            preserve_default=False,
        ),
    ]
