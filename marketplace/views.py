from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
from django.http import Http404
from marketplace.models import Product, PurchaseLog, UserProduct, Customer
from django.utils import timezone
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm
from paypal.standard.forms import PayPalPaymentsForm
from django.views.decorators.csrf import csrf_exempt
from .forms import *
from django.contrib.auth.models import User
# Create your views here.

from .forms import CreateUserForm


def index(request):
    products = Product.objects.all().order_by('-posted_at')
    if request.user.is_authenticated:
        user = User.objects.get(username=request.user.username)
        customer = Customer.objects.get(user=user)
        context = {"products": products, "user" : user, "customer" : customer }
    else:
        context = {"products": products}
    return render(request, 'marketplace/index.html', context)


def register(request):
    form = CreateUserForm()

    if request.method == "POST":
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            user_id = user.id
            return redirect('customer_info',user_id)
    context = {'form': form}
    return render(request, 'registration/register.html', context)


def customer_info(request, user_id):
    try:
        user = User.objects.get(pk=user_id)
    except User.DoesNotExist:
        raise Http404('user does not exist')
    if request.method == "POST":
        customer = Customer(user=user, name=request.POST['address'], address=request.POST['address'],
                            business_email=request.POST['business_email'])
        customer.save()
        return redirect('login')
    return render(request, 'marketplace/customer_info.html')

def add(request):
    if not request.user.is_authenticated:
        index(request)
        return 

    user = request.user
    customer = Customer.objects.get(user=user)
    customer_id = customer.id
    if request.method == 'POST':
        form = AddProductForm(request.POST, request.FILES)
        if form.is_valid():
            product = form.save()
            product_id = product.id
            user_product = UserProduct(product=Product.objects.get(pk=product_id),
                                       customer=Customer.objects.get(pk=customer_id))
            user_product.save()
            context = {'product': product, 'seller': user, 'user': user}
            return render(request, 'marketplace/product_detail.html', context)
    else:
        form = AddProductForm()
        return render(request, 'marketplace/addproduct.html', {'form': form})


def purchaselog(request, purchase_id):
    try:
        purchaselog = PurchaseLog.objects.get(pk=purchase_id)
    except PurchaseLog.DoesNotExist:
        raise Http404(' does not exist')
    context = {"purchaselog": purchaselog}
    return render(request, 'marketplace/purchaselog_detail.html', context)


def profile(request):
    user = User.objects.get(username=request.user.username)
    customer = Customer.objects.get(user=user)
    user_products = UserProduct.objects.filter(customer=customer)
    selling_lists = PurchaseLog.objects.filter(seller=customer)
    buying_lists = PurchaseLog.objects.filter(buyer=customer)
    products = []
    for user_product in user_products:
        products.append(user_product.product)
    context = {'user': user, 'customer': customer, 'products': products, 'selling_lists': selling_lists,
               'buying_lists': buying_lists}
    return render(request, 'marketplace/profile.html', context)

def profile_update(request):
    user = User.objects.get(username=request.user.username)
    customer = Customer.objects.get(user=user)
    id = customer.id
    if request.method == 'POST':
        customer = Customer( id = id, user = user, name = request.POST['name'], address=request.POST['address'],
                            business_email=request.POST['business_email'])
        customer.save()
        return redirect(profile)
    return render(request, 'marketplace/profile_update.html')


def detail(request, product_id):
    product = Product.objects.get(pk = product_id)
    seller = UserProduct.objects.get(product=product).customer.user
    seller_customer = Customer.objects.get(user=seller)
    if request.user.is_authenticated:
        user = User.objects.get(username=request.user.username)
        context = {
            "user": user,
            "product": product,
            "seller": seller,
            "seller_customer": seller_customer
        }
        return render(request, 'marketplace/product_detail.html', context)
    else:
        context = {
            "product": product,
            "seller": seller,
            "seller_customer": seller_customer
        }
        return render(request, 'marketplace/product_detail.html', context)

def product_delete(request, product_id):
    if not request.user.is_authenticated:
        index(request)
        return
    product = Product.objects.get(pk = product_id)
    product.delete()
    return redirect('index')


def checkout(request,product_id):    
    if not request.user.is_authenticated:
        index(request)
        return 
    try:
        product = Product.objects.get(pk = product_id)
    except Product.DoesNotExist:
        raise Http404('product does not exist')
    user = User.objects.get(username=request.user.username)
    customer = Customer.objects.get(user = user)
    customer_id = customer.id        
    seller = UserProduct.objects.get(product = product).customer
    business_email = seller.business_email
    if request.method == "POST":
        if business_email :
            purchase_log = PurchaseLog(
                seller = seller,
                buyer = customer, 
                product = product, 
                posted_at = timezone.now(),
                purchase_number = request.POST['purchase_number'], 
                shipping_address = request.POST['address'], 
                shipping_method = request.POST['shipping_method'],
                payment_method = request.POST['payment_method'], 
                notes = request.POST['notes'], 
                sum = int(request.POST['purchase_number']) * int(product.price))
            purchase_log.save()
        else:
            purchase_log = PurchaseLog(
                seller = seller,
                buyer = customer, 
                product = product, 
                posted_at = timezone.now(),
                purchase_number = request.POST['purchase_number'], 
                shipping_address = request.POST['address'], 
                shipping_method = request.POST['shipping_method'],
                payment_method = 'by cash', 
                notes = request.POST['notes'], 
                sum = int(request.POST['purchase_number']) * int(product.price))
            purchase_log.save()
        context = { 'purchase_log':purchase_log, 'product':product }
        return render(request,'marketplace/confirm.html',context )
    context = { "product" : product, 'business_email':business_email }
    return render(request, 'marketplace/checkout.html',context)

def confirm(request, purchase_id):
    try:
        purchase_log = PurchaseLog.objects.get(pk=purchase_id)
    except PurchaseLog.DoesNotExist:
        raise Http404('product does not exist')
    product = purchase_log.product
    context = {
        "product": product,
        "purchase_log": purchase_log
    }
    return render(request, 'marketplace/confirm.html', context)


def purchaselog_delete(request, purchase_id):
    try:
        purchaselog = PurchaseLog.objects.get(pk=purchase_id)
    except PurchaseLog.DoesNotExist:
        raise Http404(' does not exist')
    product = purchaselog.product
    user = purchaselog.buyer.user
    purchaselog.delete()
    context = {'product': product, 'user': user}
    return render(request, 'payment/order_cancelled.html', context)


def purchase(request, purchase_id):
    try:
        purchase_log = PurchaseLog.objects.get(pk=purchase_id)
    except PurchaseLog.DoesNotExist:
        raise Http404(' order does not exist')
    purchase_id = purchase_log.id
    seller = purchase_log.seller
    user = purchase_log.buyer
    product = purchase_log.product
    product.quantity -= purchase_log.purchase_number
    product.save()
    if purchase_log.payment_method == 'paypal':
        paypal_dict = {
            "business": seller.business_email,
            "amount": purchase_log.sum,
            "item_name": purchase_log.product.name,
            "invoice": purchase_id,
            "currency_code": 'USD',
            "notify_url": request.build_absolute_uri(reverse('paypal-ipn')),
            "return": request.build_absolute_uri(reverse('done', args=[purchase_id])),
            "cancel_return": request.build_absolute_uri(reverse('cancelled', args=[purchase_id])),
        }
        form = PayPalPaymentsForm(initial=paypal_dict)
        context = {"form": form}
        return render(request, "payment/paypal_payment.html", context)
    else:
        user = purchase_log.buyer.user
        context = {'user': user, 'product': product}
        return render(request, 'payment/ordersent.html', context)


@csrf_exempt
def payment_done(request, purchase_id):
    try:
        purchase_log = PurchaseLog.objects.get(pk=purchase_id)
    except PurchaseLog.DoesNotExist:
        raise Http404('does not exist')
    product = purchase_log.product
    user = purchase_log.buyer.user
    context = {'product': product, 'user': user}
    return render(request, 'payment/done.html', context)


@csrf_exempt
def payment_cancelled(request, purchase_id):
    try:
        purchase_log = PurchaseLog.objects.get(pk=purchase_id)
    except PurchaseLog.DoesNotExist:
        raise Http404('does not exist')
    product = purchase_log.product
    user = purchase_log.buyer.user
    context = {'product': product, 'user': user}
    return render(request, 'payment/cancelled.html', context)
