from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.
class Product(models.Model):
    name = models.CharField(max_length=200)
    desc = models.TextField()
    price = models.IntegerField()
    quantity = models.IntegerField(default=1)
    posted_at = models.DateTimeField(default=timezone.now)
    published_at = models.DateTimeField(blank=True, null=True)
    product_Main_Img = models.ImageField(upload_to='images/', default='default-image.jpg')
    def publish(self):
        self.published_at = timezone.now()
        self.save()
    def __str__(self):
        return self.name


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.TextField()
    address = models.TextField()
    business_email = models.EmailField(blank=True, null=True)


class PurchaseLog(models.Model):
    buyer = models.ForeignKey(Customer, related_name = 'purchaselog_buyer', on_delete = models.CASCADE)
    seller = models.ForeignKey(Customer, related_name = 'purchaselog_seller', on_delete = models.CASCADE)
    product = models.ForeignKey(Product, related_name = 'purchase_log', on_delete = models.CASCADE)
    posted_at = models.DateTimeField(default = timezone.now)
    purchase_number = models.IntegerField(default = 1)
    shipping_address = models.TextField()
    shipping_method = models.TextField()
    payment_method = models.TextField()
    notes = models.TextField()
    sum = models.IntegerField()


class UserProduct(models.Model):
    customer = models.ForeignKey(Customer, related_name='user_product', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='user_product', on_delete=models.CASCADE)
