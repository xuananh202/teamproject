# forms.py 
from django import forms 
from .models import *
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class AddProductForm(forms.ModelForm): 
  
    class Meta: 
        model = Product 
        fields = ['name', 'desc', 'price', 'quantity', 'product_Main_Img']

class CreateUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']
